# clojure

Supported environment is Ubuntu Linux.
Supported build tool is clj as described here https://clojure.org/guides/deps_and_cli. 
Default Editor is Intellij with Cursive plugin.

## Installing

Official documentation https://clojure.org/guides/getting_started.

```
sudo apt -y install bash curl rlwrap openjdk-8-jdk
curl -O https://download.clojure.org/install/linux-install-1.10.0.442.sh
chmod +x linux-install-1.10.0.442.sh
sudo ./linux-install-1.10.0.442.sh
```

## Setup for using Clojure git deps with private repositories

Add Git servers (or any other server with private repositories like github.com or bitbucket.com) to the known hosts, e.g.:
```
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
ssh-keyscan github.com >> ~/.ssh/known_hosts
ssh-keyscan bitbucket.com >> ~/.ssh/known_hosts
```

And just to be safe run this:
```
eval $(ssh-agent -s)
ssh-add ~/.ssh/id_rsa
```

## Common tasks

Run main function from some namespace:
```
clj -m my-package.my-namespace 123 234
```

Run tests:
```
clj -A:test
```

Run a portion of selected tests:
```
clj -A:test -e :integration
```

## Use in CI

Default docker container should be `clojure:tools-deps-alpine`.

When using Git Deps from private repositories we need to setup a separate build container with a proper ssh configuration.

That can be done using a `Dockerfile.ssh` in this repo.
```
docker build -f Dockerfile.ssh --build-arg ssh_prv_key="$(cat ~/.ssh/id_rsa)" --build-arg ssh_pub_key="$(cat ~/.ssh/id_rsa.pub)" -t registry.gitlab.com/jocas/clojure:ssh .
```

To create a custom image in your project demo dockerfile:
```
FROM registry.gitlab.com/jocas/clojure:ssh

COPY deps.edn deps.edn
RUN clojure -Sforce -e "(System/exit 0)"

RUN clojure -e "(require '[clojure.string :as s]) (println (t/trim \" Ciao! \"))"
```
 (Lesson learned)[https://stackoverflow.com/a/48876887/1728133].

To build your demo docker:
```
docker build -f Dockerfile.clj-demo -t clj-demo .
```

## Scripting with clojure CLI

Create a file `script.sh` with contents:

```
#!/bin/sh

"exec" "clj" "-Sdeps" "{:deps,{hiccup,{:mvn/version,\"1.0.5\"}}}" "$0" "$@"

(ns my-script
  (:require
    [hiccup.core :as hiccup]))

(println
  (hiccup/html
    [:div
      [:span "Command line args: " (clojure.string/join ", " *command-line-args*)]
      [:span "Stdin: " (read-line)]]))
```

Then:
```
chmod +x script.sh
echo "stdin" | ./script.sh command line args
# => <div><span>Command line args: command, line, args</span><span>Stdin: stdin</span></div>
# => ./script.sh command line args  2.61s user 0.10s system 279% cpu 0.972 total
```
