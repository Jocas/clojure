#!/bin/sh

"exec" "clj" "-Sdeps" "{:deps,{hiccup,{:mvn/version,\"1.0.5\"}}}" "$0" "$@"

(ns my-script
  (:require
    [hiccup.core :as hiccup]))

(println
  (hiccup/html
    [:div
      [:span "Command line args: " (clojure.string/join ", " *command-line-args*)]
      [:span "Stdin: " (read-line)]]))
      